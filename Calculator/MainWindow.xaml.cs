﻿using Calculator.ViewModels;
using System.Windows;
using System.Windows.Controls;

namespace Calculator
{
    /// <summary>
    /// Logika interakcji dla klasy MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private string _operator;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = new MainViewModel();
        }

        private void Digit_Clicked(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null || button.Content == null)
                return;

            if (_operator == null)
            {
                Screen.Text += button.Content.ToString();
                Result.Text = Screen.Text.Substring(1);
            }
            else
            {
                Screen2.Text += button.Content.ToString();
                Result.Text = Screen2.Text.Substring(1);
            }
        }

        private void Operator_Clicked(object sender, RoutedEventArgs e)
        {
            Button button = sender as Button;
            if (button == null || button.Content == null)
                return;
            _operator = button.Content.ToString();
            Operator.Text = _operator;
            Result.Text = "";
        }

        private void Equals_Clicked(object sender, RoutedEventArgs e)
        {
            _operator = null;
        }
    }
}
