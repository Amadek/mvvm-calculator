﻿using System;

namespace Calculator.Models
{
    class CoreModel
    {
        public float Calculate(int a, int b, string op)
        {
            switch (op)
            {
                case "+":
                    return a + b;
                case "-":
                    return a - b;
                case "*":
                    return a * b;
                case "/":
                    if (b == 0)
                        throw new DivideByZeroException();
                    return a / (float)b;
                default:
                    return 0;
            }
        }
    }
}
