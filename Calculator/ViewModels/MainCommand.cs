﻿using System;
using System.Windows.Input;

namespace Calculator.ViewModels
{
    class MainCommand : ICommand
    {
        public event EventHandler CanExecuteChanged;
        private Action _execute;

        public MainCommand(Action execute)
        {
            if (execute == null)
                throw new ArgumentNullException("execute");
            _execute = execute;
        }

        public bool CanExecute(object parameter)
        {
            return true;
        }

        public void Execute(object parameter)
        {
            _execute.Invoke();
        }
    }
}
