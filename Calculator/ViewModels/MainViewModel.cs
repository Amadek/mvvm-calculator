﻿using System;
using Calculator.Models;

namespace Calculator.ViewModels
{
    class MainViewModel : ObservableObject
    {
        #region FirstValue Property

        private string _firstValue;
        public string FirstValue
        {
            get
            {
                if (string.IsNullOrEmpty(_firstValue))
                    return "0";
                return _firstValue;
            }
            set
            {
                _firstValue = value;
                OnPropertyChanged("FirstValue");
            }
        }

        #endregion

        #region SecondValue Property

        private string _secondValue;
        public string SecondValue
        {
            get
            {
                if (string.IsNullOrEmpty(_secondValue))
                    return "0";
                return _secondValue;
            }
            set
            {
                _secondValue = value;
                OnPropertyChanged("SecondValue");
            }
        }

        #endregion

        #region OperatorValue Property

        private string _operatorValue;
        public string OperatorValue
        {
            get
            {
                if (string.IsNullOrEmpty(_operatorValue))
                    return "";
                return _operatorValue;
            }
            set
            {
                _operatorValue = value;
                OnPropertyChanged("OperatorValue");
            }
        }

        #endregion

        #region ResultValue Property

        private string _resultValue;
        public string ResultValue
        {
            get
            {
                if (string.IsNullOrEmpty(_resultValue))
                    return "0";
                return _resultValue;
            }
            set
            {
                _resultValue = value;
                OnPropertyChanged("ResultValue");
            }
        }

        #endregion

        public MainCommand MainCMD { get; private set; }

        private CoreModel _coreModel;

        public MainViewModel()
        {
            _coreModel = new CoreModel();

            MainCMD = new MainCommand(() =>
            {
                try
                {
                    ResultValue = _coreModel.Calculate(Int32.Parse(FirstValue), Int32.Parse(SecondValue), OperatorValue).ToString();
                }
                catch (DivideByZeroException)
                {
                    ResultValue = "DivideByZero Error";
                }
                FirstValue = SecondValue = "0";
                OperatorValue = "";
            });
        }
    }
}
